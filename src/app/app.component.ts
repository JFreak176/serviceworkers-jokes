import {Component, OnInit} from '@angular/core';
import {DataService} from './data.service';
import {SwUpdate} from '@angular/service-worker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'jokes';
  joke: any;

  constructor(private data: DataService, updates: SwUpdate) {
    updates.available.subscribe(event => {
      console.log('current version is', event.current);
      console.log('available version is', event.available);
      if (confirm('Do you want to refresh to the new version of awesome jokes?')) {
        updates.activateUpdate().then(() => console.log('update activated'));
      }
    });
    updates.activated.subscribe(event => {
      console.log('old version was', event.previous);
      console.log('new version is', event.current);
      document.location.reload();
    });
  }

  ngOnInit(): void {
    this.data.gimmeJokes().subscribe(res => {
      this.joke = res;
    });
  }
}
